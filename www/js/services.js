angular.module('photoApp.services', [])

.factory('PhotoLibraryService', function($http) {

  var photosPromise;
  return {
    getPhotos: function(){
      if (!photosPromise) {
        photosPromise = $http.get('data/photos.json')
        .then(function onFulfilled(response){
          return response.data;
        });
      }
      return photosPromise;
    },
    getPhoto: function(photoId){
      return photosPromise.then(function onFulfilled(photos){
          return photos.filter(
            function isMatching(photo) {
          return photo.id === photoId;
        })[0];
      });
    },
    deletePhoto: function(photoId){
      photosPromise = photosPromise.then(
        function onFulfilled(photos){
          return photos.filter(
            function isMatching(photo) {
              return photo.id !== photoId;
            }
          );
        });
    },
    addPhoto: function(newPhoto){
      photosPromise = photosPromise
        .then(function onFulfilled(photos){
          photos.push(newPhoto);
          return photos;
        });
    }
  }
});
