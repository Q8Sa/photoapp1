// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('photoApp', ['ionic', 'photoApp.services', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('photos', {
    url: '/',
    templateUrl: 'templates/photos.html',
    controller: 'PhotosCtrl'
  })
  .state('photo', {
    url: '/photo/:photoid',
    templateUrl: 'templates/photo.html',
    controller: 'PhotoCtrl'
  });
})

.controller('PhotosCtrl', function ($scope, $state, $cordovaCamera, $cordovaFlashlight, PhotoLibraryService) {
  $scope.$on('$ionicView.beforeEnter',
  function beforeEnter() {
    PhotoLibraryService.getPhotos().then(function(photos){
      $scope.photos = photos.slice(0);
    });
  });


  $scope.takePhoto = function(){
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options)
      .then(function(pictureFileUri){
        uploadNewPhoto(pictureFileUri);
      }, function(error){
        console.log(error);
      });
  };

//Problem sending id from controller to services
  // $scope.deletePhoto = function(photoid){
  //   var id = photoid;
  //   console.log(id);
  //   PhotoLibraryService.deletePhoto(id);
  //   $state.go('photos');
  // };

  function uploadNewPhoto(url){
    var date = new Date();
    var title = date.toISOString();
    title = title.slice(0, 10).replace(/\-/g, ".") + ' at ' + title.slice(11, 19).replace(/:/g, ".");

    var photo = {
      id: date.getTime().toString(),
      title: title,
      date: date,
      thumbnail_url: url
    };

    $scope.photos.push(photo);
    PhotoLibraryService.addPhoto(photo);
  }
})

.controller('PhotoCtrl', function($scope, $state, PhotoLibraryService) {
  var photoId = $state.params.photoid;
  PhotoLibraryService.getPhoto(photoId).then(function(photo){
    $scope.photo = photo;
  });
  $scope.deletePhoto = function(){
    PhotoLibraryService.deletePhoto(photoId);
    $state.go('photos');
  }
});
